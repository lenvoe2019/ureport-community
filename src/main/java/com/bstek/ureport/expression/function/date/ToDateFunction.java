package com.bstek.ureport.expression.function.date;

import java.util.List;

import com.bstek.ureport.build.Context;
import com.bstek.ureport.exception.ReportComputeException;
import com.bstek.ureport.expression.function.Function;
import com.bstek.ureport.expression.model.data.ExpressionData;
import com.bstek.ureport.model.Cell;
import com.bstek.ureport.utils.DataUtils;
import com.bstek.ureport.utils.DateUtils;
import com.bstek.ureport.utils.StringUtils;

public class ToDateFunction implements Function {

	@Override
	public Object execute(List<ExpressionData<?>> params, Context context, Cell currentCell) {
		if (params == null || params.size() != 2) {
			throw new ReportComputeException("todate函数需要两个参数");
		}
		Object value = DataUtils.getSingleExpressionData(params.get(0));
		String format = StringUtils.toTrimString(DataUtils.getSingleExpressionData(params.get(1)));
		return DateUtils.parseDate(value, format);
	}

	@Override
	public String name() {
		return "todate";
	}
}
